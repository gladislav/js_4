// 1. Функція групує у себе певний код, який можна визивати в різних частинах программи, що в свою чергу 
//    полегшує програмувааня, та робить программу більш легкою для розуміння.
// 2. Аргументи - це значення, які передаються у функцію для виконання певної операці, наприклад кількість 
//    повторень.
// 3. Оператор "return" використовується для повернення результату з функції, тобто виконанні функціі ми отримуємо
//    якись результат який ми можемо використати далі у программі, після оператора "return" виконання функціі 
//    завершується.
const num1 = parseFloat(prompt('Enter the first number:'));
const num2 = parseFloat(prompt('Enter the second number:'));
const operator = prompt('Enter the operator (+, -, *, /):');
function calculate(num1, num2, operator) {
    let result;
      switch (operator) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        result = num1 / num2;
        break;
      default:
        console.log('Invalid operator');
        return;
    }
      console.log(`${num1} ${operator} ${num2} = ${result}`);
  }
  calculate(num1, num2, operator);